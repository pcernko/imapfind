# IMAPFind

A find-like tool for your IMAP mailbox.


## Description

`imapfind` allows you to search your IMAP mailbox using search
filters. You can display the results and start several actions on
them. Displaying can be customized by selecting a list of fields from
each mail like headers, body text, attachments or size. Actions
include moving, deleting and flagging.

Search filters can be combined using boolean operators.

## Installation

`imapfind` is a Python script that uses the following additional modules:
 * [imap_tools](https://github.com/ikvk/imap_tools)
 * [boolean_parser](https://github.com/havok2063/boolean_parser)

## Usage

 0. Find all mails in your INBOX with Subject *Fwd:* and display them:
    ```sh
    imapfind --server mail.example.com subject=Fwd:
     ```
 0. Show and delete spam mails in folder *Junk*:
    ```sh
    imapfind --server mail.example.com --folder Junk subject=spam --print --delete
    ```
 0. List available folder
    ```sh
    imapfind --server mail.example.com --list-folders subject='' # search filter is required although not used with --list-folders
    ```
 0. Move mails from postmaster to Trash folder
    ```sh
    imapfind from_=postmaster --move Trash
    ```
 0. Assign Thunderbird label *To Do* to unread mails
    ```sh
    imapfind seen=False --flags '$label4'
    ```
 0. Save attachments of bills from Amazon
    ```sh
    imapfind subject=bill and from_=amazon --save-attachments
    ```
 0. simple daemon to forward spam to learning service
    ```sh
    imapfind \
        --folder SPAM.unrecognized no_keyword='$Forwarded' \
        --print \
        --forward learn-spam@example.com --bcc $USERNAME@example.com \
        --flag '$Forwarded' \
        --loop 60
    ```
    This command will scan the folder `SPAM.unrecognized` for mails
    not yet marked as forwarded (e.g. by Thunderbird). The found mails
    will be printed with default information (uid, date, flags,
    subject), then forwarded to `learn-spam@example.com` with bcc to
    yourself. After that, they will be marked as forwarded. This
    serves two purposes: You have a visual feedback in your mail
    client, that the mails got handled and they are not handled again
    in the next iteration. When all this is done, `imapfind` sleeps
    for 60 seconds and then repeats search and actions again until
    terminated by `CTRL-C`.

### Environment variables

The following environment variables are used:
 * `IMAPFIND_SERVER`: The mail server to use, if `--server` not given.
 * `IMAPFIND_USERNAME`: The username to use, if `--username` not given.
 * `IMAPFIND_PASSWORD`: The password to access the IMAP mailbox, to avoid getting prompted.
 * `IMAPFIND_PW_COMMAND`: Instead of storing the password, you can also specify a command, that prints the password, [see below](#password-command)
 * `IMAPFIND_FOLDER`: The IMAP folder to use, if `--folder` not given.
 * `IMAPFIND_ATTACHMENT_FILENAME`: Attachment filename pattern, see section [filename patterns](#filename-patterns)
 * `IMAPFIND_SMTP_SERVER`: The SMTP server to use (for forwarding), if `--smtp-server` not given.
 * `IMAPFIND_SUBJECT`: The subject to use (for forwarded mails), if `--subject` not given.
 * `IMAPFIND_SENDER`: The sender address to use (for forwarded mails), if `--sender` not given.
 * `IMAPFIND_BCC`: Send mails as blind carbon copy to these addresses, if `--bcc` not given.
 * `IMAPFIND_BULK`: How many mails to handle in each bulk, if `--bulk` not given, see section [bulk support](#bulk-support)

Additionally the default for `--username` is determined from Python's
[getpass.getuser function](https://docs.python.org/3/library/getpass.html#getpass.getuser)
if `IMAPFIND_USERNAME` is not set.

#### <a name="password-command"></a>Password command

Storing a password in an environment variable is unsafe. Therefore,
`imapfind` allows you to provide a command/script that will print the
password. The command is invoked with `<username>@<server>` as
parameter to allow you to print different passwords depending on the
server and username. Feel free to connect to your password safe to
fetch the corresponding password.

### forward action

The action `--forward` requires a list of email addresses. The list
can be seperated by whitespace (beware of shell quoting!) or comma.

All found mails are send as attachments (Content-Type
*message/rfc822*) in **one** mail. Use the searchfilter to limit the
number of mails appropriately. If not subject given with `--subject`
or `IMAPFIND_SUBJECT`, the subject is computed from the first found
mail's subject prefixed by "Fwd: ".

If not specified with `--smtp-server` or `IMAPFIND_SMTP_SERVER` the
mail is submited via SMTP to `localhost`. SMTP-Auth is not supported
yet. The sender address can be set with `--sender` or
`IMAPFIND_SENDER` or the following default is computed: The local part
of the email address is taken from the username for the IMAP
connection and the domain part is computed as the domain of the
IMAP server (aka trunkated by last subdomain part). If you
want to have some recipients as BCC, you can use `--bcc` or
`IMAPFIND_BCC` with the same format as for `--forward`.


### <a name="filename-patterns"></a>Filename patterns

The `--save-attachments` option can accept an optional argument. This
argument specifies a Python format string, to generate filenames for
the saved attachments.

If not specified, a internal default is used or the content of the
`IMAPFIND_ATTACHMENT_FILENAME` environment variable. The internal default is:
```
%(allcount)03d %(date)s %(subject)s %(filename)s
```
Example result: `003 2024-06-05 14:33:37+02:00 Fwd: payment invoice.pdf`

Available mapping keys for the format string:
 * `allcount` counter of all saved attachments of all handled mails
 * `count` counter of attachments of current mail
 * `uid` IMAP uid of message
 * `to` message's `To:` headers joined by space
 * `date` message's date (as Python `datetime` object)
 * `subject` message's subject with `\n` and `\r` removed and `/` replaced by `_`
 * `filename` attachment filename as specifed in the MIME metadata with `\n` and `\r` removed and `/` replaced by `_`
 * `content_type` the content type of the attachment as specifed in the MIME metadata

See [Python
documentation](https://docs.python.org/3/library/stdtypes.html#printf-style-string-formatting)
for full documentation of printf style string formatting.


### Search filters

#### Criteria

`imapfind` relys on the criterias provided by `imap_tools`. See
https://github.com/ikvk/imap_tools?tab=readme-ov-file#search-criteria
or `pydoc imap_tools.AND` for a detailed documentation. Any key-value
parameter of `imap_tools.AND` can be used.

To specify headers, you can use `h:<HEADER-NAME>` as key. The strings
`true`, `false`, `yes` and `no` get converted to Python booleans. All
values for date criteria are converted to Python date oject using
`datetime.date.fromisoformat()`.

#### Boolean expressions

Thanks to `boolean_parser`, you can specify complex expressions using
operators `and`, `or`, `not` and brackets (`(` and `)`). Note: On most
shells, brackets need to be quoted!

Example:
```sh
subject=spam and '(' not from_='amazon store' or date_lt=2024-01-01 \)
```
*Mails with subject "spam" that are either not from "amazon store" or older than 2024.**


### <a name="bulk-support"></a>Bulk support

`imap_tools` version 1.9.1 only had `bulk` support for fetching
mails. I have submitted a [pull
request](https://github.com/ikvk/imap_tools/pull/253) for `bulk`
support in `copy`, `move`, `delete` and `flag`. This was integrated
with some modifications by the main author in version 1.10.0 of
`imap_tools`.

`imapfind` will test, if the used `imap_tools` have support for `bulk`
on actions and use a non-bulk call if not.


## Contributing

Patches and pull requests are welcome.

## Acknowledgments

Thanks to [Vladimir Kaukin](https://github.com/ikvk) for providing the great Python module `imap_tools` and to [Brian Cherinka](https://github.com/havok2063) for his `boolean_parser`.

## License

GPL.
